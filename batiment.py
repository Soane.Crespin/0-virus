import json

def sauvegarder_donnees(monTableau):
    fichier = open(file="Data/batiments.json", mode="w")
    json.dump(monTableau, fichier, indent=4)
    fichier.close()

def charger_donnees_batiments()->dict:
    fichier = open(file="Data/batiments.json", mode="r")
    mesDonnees = json.load(fichier)
    fichier.close()
    return mesDonnees


class Batiment():
    def __init__(self, type_batiment:str):
        """Quelques explications ici SVP"""
        DICO_BATIMENT = charger_donnees_batiments()

        self.nom    = type_batiment
        print(DICO_BATIMENT.keys())
        self.argent = DICO_BATIMENT[type_batiment]["argent"]
        self.bois   = DICO_BATIMENT[type_batiment]["bois"]
        self.pierre = DICO_BATIMENT[type_batiment]["pierre"]
        self.nourriture = DICO_BATIMENT[type_batiment]["nourriture"]

        self.troupe = DICO_BATIMENT[type_batiment]["troupe"]
        self.scientifique = DICO_BATIMENT[type_batiment]["scientifique"]
        self.population = DICO_BATIMENT[type_batiment]["pop"]

        self.price = DICO_BATIMENT[type_batiment]['prix']

        self.environnement = DICO_BATIMENT[type_batiment]["environnement"]

    def recolter(self):
        """Renvoie les ressources gÃ©rÃ©es en fin de tour"""
        return self.argent, self.bois, self.pierre, self.nourriture

    def prix_batiment(self):
        """renvoie self.price"""
        return self.price

    def __str__(self):
        txt = self.nom
        txt += "\n Prix: "+str(self.price)
        txt += " \n Production:"+" argent: "+str(self.argent)+","
        txt +=  " bois: "+str(self.bois)+","
        txt +=  " pierre: "+str(self.pierre)+","
        txt +=  " nourriture: "+str(self.nourriture)
        return txt

    def __repr__(self):
        return self.nom




############
### MAIN ###
############


if __name__ == '__main__':
    t = Batiment("Scierie")
    print(t)
