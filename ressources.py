class Ressources():
    def __init__(self, bois=0, pierre=0, argent=0, nourriture=0)->int:
        self.stock_bois       = bois
        self.stock_pierre     = pierre
        self.stock_argent     = argent
        self.stock_nourriture = nourriture


    ### FONCTIONS POUR AJOUTER DES RESSOURCES ###
    def ajouter_bois(self, bois):
        self.stock_bois       += bois

    def ajouter_pierre(self, pierre):
        self.stock_pierre     += pierre

    def ajouter_argent(self, argent):
        self.stock_argent     += argent

    def ajouter_nourriture(self, nourriture):
        self.stock_nourriture += nourriture

    def ajouter_ressources(self, bois:int=0, argent:int=0, pierre:int=0, nourriture:int=0):
        """Utiliser les autres fonctions ajouter_..."""
        self.ajouter_nourriture(nourriture)
        self.ajouter_pierre(pierre)
        self.ajouter_bois(bois)
        self.ajouter_argent(argent)

    ### FONCTIONS POUR RETIRER DES RESSOURCES ###
    def retirer_bois(self,bois:int):
        if self.stock_bois >= bois :
            self.stock_bois -= bois

    def retirer_pierre(self,pierre:int):
        if self.stock_pierre >= pierre :
            self.stock_pierre -= pierre

    def retirer_argent(self,argent:int):
        if self.stock_argent >= argent :
            self.stock_argent -= argent

    def retirer_nourriture(self,nourriture:int):
        if self.stock_nourriture > nourriture :
            self.stock_nourriture -= nourriture

    def retirer_ressources(self, bois:int=0, argent:int=0, pierre:int=0, nourriture:int=0):
        """Utiliser les autres fonctions retirer_..."""
        self.retirer_argent(argent)
        self.retirer_bois(bois)
        self.retirer_pierre(pierre)
        self.retirer_nourriture(nourriture)

    ### FONCTIONS POUR SAVOIR S'IL Y A ASSEZ DE RESSOURCES ###
    def peut_retirer_bois(self, bois:int)->bool:
        """renvoie True ou False selon qu'il y a assez de bois ou pas"""
        if self.stock_bois >= bois:
            return True
        else:
            return False


    def peut_retirer_pierre(self, pierre:int)->bool:
        """renvoie True ou False selon qu'il y a assez de pierre ou pas"""
        if self.stock_pierre >= pierre:
            return True
        else:
            return False
    def peut_retirer_argent(self,argent:int)->bool:
        """renvoie True ou False selon qu'il y a assez de argent ou pas"""
        if self.stock_argent >= argent:
            return True
        else:
            return False
    def peut_retirer_nourriture(self,nourriture:int)->bool:
        """renvoie True ou False selon qu'il y a assez de nourriture ou pas"""
        if self.stock_nourriture >= nourriture:
            return True
        else:
            return False

    def peut_retirer_ressources(self, bois:int=0, argent:int=0, pierre:int=0, nourriture:int=0)->bool:
        """renvoie True ou False selon qu'il y a assez de ressources ou pas"""
        res= True

        if self.peut_retirer_bois(bois)== False:
            res= False
        if self.peut_retirer_argent(argent)==False:
            res= False
        if self.peut_retirer_pierre(pierre)==False:
            res=False
        if self.peut_retirer_nourriture(nourriture)==False:
            res= False
        return res

    ### FONCTIONS D'AFFICHAGE
    def __str__(self):
        """Renvoie un texte pour présenter l'ensemble des ressources"""
        txt= "Vous avez " +str(self.stock_argent)+" argents.\n"
        txt+= "Vous avez " +str(self.stock_pierre)+" pierres.\n"
        txt+= "Vous avez " +str(self.stock_bois)+" bois.\n"
        txt+= "Vous avez " +str(self.stock_nourriture)+" nourritures."
        return txt

    def __repr__(self):
        return str(self)


############
### MAIN ###
############

if __name__ == '__main__':
    mes_ressources = Ressources()

    mes_ressources.ajouter_bois(50)
    mes_ressources.ajouter_ressources(argent=100, pierre=75)
    mes_ressources.retirer_nourriture(40)
    mes_ressources.retirer_pierre(10)

    print(mes_ressources)
