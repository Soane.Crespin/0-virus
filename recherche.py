from random import randint

"""PROPOSITION : 
    - DONNEES : stocker la liste des virus, leur nom et leur coût dans un fichier JSON
        prévoir s'il y a des dépendances (exemple : SARS-COV2 nécessite SARS-COV1)
    - ATTRIBUTS : 
        - self.scientist : initialiser à 0 ou 1
        - self.nom_virus : nom du virus en cours de recherche
        - self.cout_recherche : initialiser en fonction du fichier JSON
        - self.liste_virus : list vide contenant les virus découverts
    - METHODES : 
        - rechercher() : diminuer self.cout_recherche de self.scientist, 
                un coefficient aléatoire peut être appliqué si vous le
                voulez (valeur : 1±coeff entre 0 et 0.5)
        - cout() : renvoie le coût de la recherche qu'il faudra payer à 
                chaque tour de jeu (dépendra de self.scientist)
        - reussite_recherche() : renvoie True si self.cout_recherche atteint 
                0 et False sinon, ajoute self.nom_virus à self.liste_virus si réussite
        - nouveau_virus() : réinitialise self.nom_virus et self.cout_recherche
        - augmenter(), diminuer() : permet de modifier le nombre de scientifiques
        - __str__() : pour connaître rapidement l'état de la recherche
"""
class Recherche_virus():
    def __init__(self, argent, scientifique):
        self.argent    = argent
        self.scientist = scientifique
        self.cout_recherche = 10000         #argent pour la recherche


    def investissement_scientifique(self):
        self.argent   -= self.cout_recherche
        if self.scientist >= 100 :
            return True
        else:
            chance_de_reussite = randint(self.scientist, 100)
            if chance_de_reussite >= (self.scientist+100) // 2:
                return True
            else:
                return False


    def reussite_recherche(self, nom_virus):
        if self.investissement_scientifique():
            #Ne faudrait-il pas diminuer le nombre de scientifiques ???
            return "votre virus {} est né".format(nom_virus)
        else:
            return "votre virus {} a échoué".format(nom_virus)



############
### MAIN ###
############

if __name__ == '__main__':
    g=Recherche_virus(20000,70)
    print(g.investissement_scientifique())
    print(g.reussite_recherche("SARS-Cov2"))