from batiment import *
from carte    import *
from soldat   import*
from joueur import *
#from unit   import *




class Jeu():
    def __init__(self, nombreJoueurs = 2, dimensionCarte = None):
        self.carte     = Carte(dimensionCarte[0], dimensionCarte[1])
        self.joueurs = []
        for i in range(nombreJoueurs):
            self.joueurs.append(Joueur("Joueur "+str(i)))

        self.nombre_de_tours = 50
        self.tour_actuel     = 0



    ### FONCTIONS GENERALES

    def fin_jeu(self)->bool:
        """
        TODO :
            - Doit renvoyer True ou False selon que le jeu est terminÃ© ou non
            (tous les joueurs sont morts, le temps est Ã©coulÃ©â€¦)"""
        if ...:
            return ...
        else:
            return ...

    def tour_joueur_IA(self, numero_joueur:int):
        pass



    ### FONCTIONS DE GESTION DES BATIMENTS ET UNITES


    def construire_batiment(self, num_joueur:int, nom_bat:str, coord:tuple):
        ligne, colonne = coord
        bat            = Batiment(nom_bat)
        if self.joueurs[num_joueur].peut_construire_batiment(bat) :
            self.joueurs[num_joueur].construire_batiment(bat)
            self.carte.ajouter_batiment(ligne, colonne, bat)
        else:
            del(bat)

    def detruire_batiment(self, coord:tuple):
        """
        WARNING : le bÃ¢timent appartient sÃ»rement Ã  un joueur !
            Il faudrait le retirer de sa liste de bÃ¢timents.
        """
        ligne, colonne = coord
        if self.carte.contient_batiment(ligne,colonne):
            self.carte.retirer_batiment(ligne,colonne)


    def ajouter_troupes(self, num_joueur:int, nom_troupe:str,coord:tuple):
        ligne, colonne = coord
        soldat = Soldat(nom_troupe)
        if self.joueurs[num_joueur].peut_acheter_troupe(soldat) :
            self.carte.ajouter_troupe(ligne, colonne, soldat)
            self.joueurs[num_joueur].produire_troupe(soldat)


    def detruire_troupe(self, coord:tuple):
        """
        WARNING :
            - vÃ©rifier qu'il y a une unitÃ© Ã  cet endroit
            - l'unitÃ© appartient sÃ»rement Ã  un joueur ! Il faudrait la retirer de sa liste de troupes.
        """
        ligne, colonne = coord
        ... #if
        self.carte.retirer_troupe(ligne,colonne)

    def deplacer_troupe(self, depart:tuple, arrivee:tuple):
        """
        WARNING :
            - vÃ©rifier que les coordonnÃ©es sont valides
            - vÃ©rifier qu'il n'y a pas une autre unitÃ© sur la case d'arrivÃ©e (sinon il y a combat)
            - ...
        """
        self.carte.deplacer_troupe(depart,arrivee)


if __name__ == '__main__':
    game = Jeu(nombreJoueurs=3, dimensionCarte=(8,12))
    game.ajouter_troupes(0, "chevalier", (0,1))
    print("liste des troupes du joueur 0 :", game.joueurs[0].troupes)

    game.construire_batiment(0, "Maison", (1,2))
    print("liste des batiments du joueur 0 :", game.joueurs[0].batiments)