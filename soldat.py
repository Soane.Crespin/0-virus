from random import randint
import json


class Soldat():
    def __init__(self, soldat):
        fichier = open(file="Data/troupe.json", mode="r")
        dico = json.load(fichier)
        fichier.close()
        self.pv_initial     = dico[soldat]['pv']
        self.pv_actuel      = dico[soldat]['pv']
        self.nom            = dico[soldat]['nom']
        self.attaque        = dico[soldat]['attaque']
        self.attaque_actuel = dico[soldat]['attaque']
        self.cout           = dico[soldat]['prix']

    ### FONCTIONS SUR L'ETAT
    def etat(self):
        if self.pv_actuel <= 0:
            txt= self.nom + " mort au combat."
        elif self.pv_actuel <= self.pv_initial//2:
            txt= self.nom + " est infecté."
            """à remplacer, elle voit si une fonction qui envoie un virus a été activé"""
        else:
            txt= self.nom + " est en bonne santé."
        return txt

    def est_mort(self)->bool:
        if self.pv_actuel <= 0:
            return True
        else:
            return False

    def est_infecte(self)->bool:
        if self.pv_actuel <= self.pv_initial//2:
            return True
        else:
            return False

    def est_en_forme(self)->bool:
        if self.etat==0:
            return True
        else:
            return False

    def __str__(self):
        pv_a = str(self.pv_actuel)
        pv_i = str(self.pv_initial)
        att  = str(self.attaque)
        txt  = "Le {0} a {1}/{2} pv.\n".format(self.nom, pv_a, pv_i)
        txt+= "Ainsi que "+ att+ " points d'attaque. \n"
        return txt

    def __repr__(self):
        return str(self)

    ### FONCTIONS DE COMBAT

    def attaque_action(self, soldat2):
        soldat2.pv_actuel = soldat2.pv_actuel - self.attaque_actuel
        if soldat2.pv_actuel < 0:
            soldat2.pv_actuel = 0


    def combat(self, soldat2):
        """WARNING :
            - ne faudrait-il pas tenir compte d'un attribut vitesse pour savoir qui a le
            plus de chance d'attaquer en premier ?
        """
        premier = randint(1,2)        #qui attaque en premier
        if premier == 1:
            self.attaque_action(soldat2)
            if not soldat2.est_mort():
                soldat2.attaque_action(self)
        else:
            soldat2.attaque_action(self)
            if not self.est_mort():
                self.attaque_action(soldat2)


    ### SOINS ET AMELIORATIONS
    def ameliorer_equipement(self):
        """WARNING : INUTILE EN L'ÉTAT"""
        difA = self.attaque_actuel - self.attaque
        #assert(difA!=6), "amélioration maximum atteinte" ON NE VA PAS FAIRE PLANTER LE JEU POUR CA !!!
        if difA == 0:
            self.attaque_actuel +=1
            txt="Votre "+self.nom+ " à maintenaint un équipement en cuivre."
        elif difA == 1:
            self.attaque_actuel +=2
            txt="Votre "+self.nom+ " à maintenaint un équipement en fer."
        else:
            self.attaque_actuel +=3
            txt="Votre "+self.nom+ " à maintenaint un équipement en platine."
        return txt

    """EST-IL NÉCESSAIRE DE RENVOYER UN TEXTE ?
    IL SUFFIRA D'UN SEUL TOUR POUR SOIGNER UN PERSONNAGE ?"""
    def soigner(self):
        pv_gagne       = self.pv_initial - self.pv_actuel
        self.pv_actuel = self.pv_initial
        txt= "Vous avez récuperé "+ str(pv_gagne)+ " points de vie."
        return txt


    def ameliorer(self, nouv_soldat:str):
        #assert(nouv_soldat!= self.nom)
        ancien_nom = self.nom
        self       = Soldat(nouv_soldat)
        txt        = "Votre "+ ancien_nom + " a évolué en "+ self.nom + "."
        return txt


if __name__ == '__main__':
    t = Soldat('chevalier')
    s = Soldat('archer')

    t.combat(s)

    print(t.etat())
    print(t)
    print(t.ameliorer_equipement())
