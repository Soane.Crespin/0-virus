from tkinter import *
from random import choice
from batiment import *
from joueur import *


class BarreBas:
    def __init__(self, master, joueur:Joueur):
        self.joueur = joueur
        self.master = master
        self.frame = Frame(master)

        if len(self.joueur.batiments) > 0:
            self.bat_actuel = self.joueur.batiments[0]
        else:
            self.bat_actuel = "Aucun batiment"

        self.img_precedent = PhotoImage(file = 'Icon/prec.png')
        self.img_suivant = PhotoImage(file = 'Icon/next.png' )


        self.label_info  = Label(
            master = self.frame,
            text= str(self.bat_actuel)
            )
        self.btn_suivant= Button(
            master = self.frame,
            image=self.img_suivant,
            #text = 'Suivant',
            command = self.btn_suivant_click
            )
        self.btn_precedent= Button(
            master = self.frame,
            image=self.img_precedent,
            #text = 'Precedent',
            command = self.btn_precedent_click)

        self.dessiner_widgets()


    def dessiner_widgets(self):
        self.btn_precedent.grid(sticky = NS , column = 0, row = 0)
        self.btn_suivant.grid(sticky = NS ,column = 2, row = 0)
        self.label_info.grid(sticky = NS ,column = 1, row = 0)


    def maj_label_info(self):
        self.label_info.configure(text = str(self.bat_actuel))

    def changer_batiment(self):
        if self.joueur.nb_batiments()>0:
            self.bat_actuel = self.joueur.batiment_actuel()
            self.maj_label_info()

    def btn_suivant_click(self):
        if self.joueur.nb_batiments()>0:
            self.bat_actuel = self.joueur.batiment_suivant()
            self.maj_label_info()

    def btn_precedent_click(self):
        if self.joueur.nb_batiments()>0:
            self.bat_actuel = self.joueur.batiment_precedent()
            self.maj_label_info()



if __name__ == '__main__':
    j = Joueur("Cro-Magnon")
    j.batiments = [Batiment("Scierie"), Batiment("QG"), Batiment("Mine")]
    j.num_bat_actuel = 0

    fenetre = Tk()
    fenetre.title("Fenetre de test")
    barre_bas = BarreBas(fenetre, j)
    barre_bas.frame.grid()
    fenetre.mainloop()