from tkinter import *
from int_generale import *
#from int_fonctions_menus import*


class InterfaceMenu():
    def __init__(self, master):
        self.master = master
        self.police = ('Arial', 80)
        self.frameActuelle = None
        self.charger_menu_accueil()
        self.afficher_menu_accueil()



    ### MENU ACCUEIL
    def on_btn_SETINGS_click(self):
        self.frameActuelle.destroy()
        self.charger_menu_options()
        self.afficher_menu_options()

    def on_btn_PLAY_click(self):
        self.frameActuelle.destroy()
        self.charger_menu_jouer()
        self.afficher_menu_jouer()

    def on_btn_RETURN_click(self):
        self.frameActuelle.destroy()
        self.charger_menu_accueil()
        self.afficher_menu_accueil()

    def on_btn_NOUV_click(self):
        self.frameActuelle.destroy()
        self.charger_menu_nouv()
        self.afficher_menu_nouv()

    def on_btn_START_click(self):
        """
        TODO :
            - fournir des paramètres au Jeu
        """
        self.frameActuelle.destroy()
        game = Jeu()
        self.frameActuelle = InterfaceGenerale(self.master, game)



    def charger_menu_accueil(self):
        self.frameActuelle = Frame(master = self.master)
        self.frameActuelle.config(bg = "#00ffcf", borderwidth = 50)

        self.btn_PLAY    = Button(
            master = self.frameActuelle,
            text = 'Jouer',
            command = self.on_btn_PLAY_click,
            #image = 'img_btn.jpg',
            #compound = LEFT
            )
        self.btn_SETINGS = Button(
            master = self.frameActuelle,
            text = 'Options',
            command = self.on_btn_SETINGS_click)
        self.btn_EXIT    = Button(
            master = self.frameActuelle,
            text = 'Quitter',
            command = self.master.destroy)

        self.btn_PLAY.config(font=self.police)
        self.btn_SETINGS.config(font=('Arial',20))
        self.btn_EXIT.config(font=('Comic sans MS',20))

    def afficher_menu_accueil(self):
        self.frameActuelle.grid(row = 0, column = 3)
        self.btn_PLAY.grid(row=1,column=1)
        self.btn_SETINGS.grid(row=2,column=1)
        self.btn_EXIT.grid(row=3,column=1)


    ### MENU OPTIONS
    def charger_menu_options(self):
        self.frameActuelle = Frame(master = self.master)
        self.frameActuelle.config(bg = "#00ffcf", borderwidth = 50)

        self.btn_SONS   = Button(master = self.frameActuelle, text = 'Son',command = self.frameActuelle.destroy)
        self.btn_VIDS   = Button(master = self.frameActuelle, text = 'Video',command = self.frameActuelle.destroy)
        self.btn_RETURN = Button(master = self.frameActuelle, text = 'Retour',command = self.on_btn_RETURN_click) #classe.organiser()) ????

        self.btn_SONS.config(font=('Arial',25))
        self.btn_VIDS.config(font=('Arial',25))
        self.btn_RETURN.config(font=('Comic sans MS',20))

    def afficher_menu_options(self):
        self.frameActuelle.grid(row = 0, column = 3)
        self.btn_SONS.grid(row=1,column=1)
        self.btn_VIDS.grid(row=2,column=1)
        self.btn_RETURN.grid(row=3,column=1)


    ### MENU JOUER
    def charger_menu_jouer(self):
        self.frameActuelle = Frame(master = self.master)
        self.frameActuelle.config(bg = "#00ffcf", borderwidth = 50)

        self.btn_NOUV   = Button(master = self.frameActuelle, text = 'Nouvelle Partie',command = self.on_btn_NOUV_click)
        self.btn_OPEN   = Button(master = self.frameActuelle, text = 'Charger une Partie',command = self.frameActuelle.destroy)
        self.btn_RETURN = Button(master = self.frameActuelle, text = 'Retour',command = self.on_btn_RETURN_click) #classe.organiser()) ????

        self.btn_NOUV.config(font=('Arial',25))
        self.btn_OPEN.config(font=('Arial',25))
        self.btn_RETURN.config(font=('Comic sans MS',20))


    def afficher_menu_jouer(self):
        self.frameActuelle.grid(row = 0, column = 3)
        self.btn_NOUV.grid(row=1,column=1)
        self.btn_OPEN.grid(row=2,column=1)
        self.btn_RETURN.grid(row=3,column=1)


    ### MENU NOUV
    def charger_menu_nouv(self):
        self.frameActuelle = Frame(master = self.master)
        self.frameActuelle.config(bg = "#00ffcf", borderwidth = 50)

        self.btn_MAP   = Button(master = self.frameActuelle, text = 'Cartes',command = self.frameActuelle.destroy)
        self.btn_DIFF   = Button(master = self.frameActuelle, text = 'Difficulte',command = self.frameActuelle.destroy)
        self.btn_START  = Button(master = self.frameActuelle, text = 'Commencer la partie',command = self.on_btn_START_click)
        self.btn_RETURN = Button(master = self.frameActuelle, text = 'Retour',command = self.on_btn_RETURN_click) #classe.organiser()) ????

        self.btn_MAP.config(font=('Arial',25))
        self.btn_DIFF.config(font=('Arial',25))
        self.btn_START.config(font=('Arial',25))
        self.btn_RETURN.config(font=('Comic sans MS',20))

    def afficher_menu_nouv(self):
        self.frameActuelle.grid(row = 0, column = 3)
        self.btn_MAP.grid(row=1,column=1)
        self.btn_DIFF.grid(row=2,column=1)
        self.btn_START.grid(row=3,column=1)
        self.btn_RETURN.grid(row=4,column=1)

    '''def organiser():



    ### MENU JOUER : !!!!!!!!!! A FINIR  !!!!!!!!!!!!!
        """self.jouer = Frame(master = master)
        self.jouer.config(cnf = None, bg = "#00ffcf", borderwidth = 50)

        self.btn_NOUV   = Button(master = jouer, text = 'Nouvelle Partie',command = jouer.destroy)
        self.btn_OPEN   = Button(master = jouer, text = 'Charger une Partie',command = jouer.destroy)
        self.btn_RETURN = Button(master = jouer, text = 'Retour',command = afficher_accueil(master))

        self.btn_NOUV.config(font=('Arial',25))
        self.btn_OPEN.config(font=('Arial',20))
        self.btn_RETURN.config(font=('Comic sans MS',20))


        self.partie = Frame(master = master)
        self.partie.config(cnf = None, bg = "#00ffcf", borderwidth = 50)

        self.btn_MAP = Button(master = partie, text = 'Cartes',command = partie.destroy)
        self.btn_DIFF = Button(master = partie, text = 'DifficultÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©',command = partie.destroy)
        self.btn_RETURN = Button(master = partie, text = 'Retour',command = afficher_accueil(master))

        self.btn_MAP.config(font=('Arial',25))
        self.btn_DIFF.config(font=('Arial',25))
        self.btn_RETURN.config(font=('Comic sans MS',20))"""




        """

        self.btn_NOUV.grid(row=1,column=1)
        self.btn_OPEN.grid(row=2,column=1)
        self.btn_RETURN.grid(row=3,column=1)

        jouer.grid(row = 0, column = 2)



        partie.grid(row = 0, column = 3)

        self.btn_MAP.grid(row=1,column=1)
        self.btn_DIFF.grid(row=2,column=1)
        self.btn_RETURN.grid(row=3,column=1)"""



    def afficher_accueil(fenetre):
        accueil = Frame(master = fenetre)
        accueil.config(cnf = None, bg = "#00ffcf", borderwidth = 50)
        accueil.grid(row = 0, column = 0)

        self.btn_PLAY = Button(master = accueil, text = 'Jouer',command = accueil.destroy)
        self.btn_SETINGS = Button(master = accueil, text = 'Options',command = accueil.destroy)
        self.btn_EXIT = Button(master = accueil, text = 'Quitter', command = fenetre.destroy)

        self.btn_PLAY.config(font=('Arial',25))
        self.btn_SETINGS.config(font=('Arial',20))
        self.btn_EXIT.config(font=('Comic sans MS',20))

        self.btn_PLAY.grid(row=1,column=1)
        self.btn_SETINGS.grid(row=2,column=1)
        self.btn_EXIT.grid(row=3,column=1)


'''

############
### MAIN ###
############

if __name__ == '__main__':
    fenetre = Tk()
    fenetre.title('PEPO')
    bg = PhotoImage(file = "Image/fenetre_bg.png")
    #fenetre.config(bg = PhotoImage(file = "fenetre_bg.png"))
    fenetre.attributes("-fullscreen", True)
    fenetre.iconbitmap(bitmap = 'icone.ico')
    #fenetre.geometry("600x400")

    menu = InterfaceMenu(fenetre)

    fenetre.mainloop()

