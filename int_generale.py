from tkinter        import *
from int_carte      import *
from int_barre_haut import *
from int_barre_bas  import *
from jeu            import *

class InterfaceGenerale:
    def __init__(self, master, jeu:Jeu):
        self.master = master
        self.police = ('Arial', 80)
        self.ressources = Ressources()

        #CARTE
        """self.zone_carte = InterfaceCarte(self.master, jeu.carte)
        self.zone_carte.ecran.grid(row=1, column=0, columnspan=3)"""

        #BARRE DU HAUT
        self.barre_haut = BarreHaut(self.master, self.ressources)
        self.barre_haut.frame.grid(row=0, column=0)

        #BARRE DU BAS
        self.barre_bas = BarreBas(self.master, jeu.joueurs[0])
        self.barre_bas.frame.grid(row=2, column=0)



if __name__ == "__main__":
    j = Jeu(2, (16, 18))
    j.joueurs[0].ressources = Ressources(1000,1000,1000,1000)
    j.construire_batiment(0,"Mine",(1,2))
    j.construire_batiment(0,"Caserne",(3,4))
    f = Tk()
    f.title("fenetre de test")
    i = InterfaceGenerale(f, j) #erreur ici !
    #print(len(j.carte.grille), len(j.carte.grille[0]), j.carte.grille[0])
    f.mainloop()