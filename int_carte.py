from tkinter import *
from carte import *
from region import *
import json

TAILLE_TUILES = 64

class InterfaceCarte():
    def __init__(self, master, carte:Carte):
        ##IMPORTER LES IMAGES
        self.images = dict()
        dico_terrains = charger_donnees_terrains()
        for nom_terrain in dico_terrains:
            self.images[nom_terrain] = PhotoImage(file='Image/'+ dico_terrains[nom_terrain]["image"])
        if "soldat" not in dico_terrains:
            self.images["soldat"] = PhotoImage(file='Image/soldat.png')

        self.carte = carte
        self.ecran = Canvas(master,
                            bg="white",
                            width=TAILLE_TUILES*self.carte.nb_colonne,
                            height=TAILLE_TUILES*self.carte.nb_ligne)
        self.ecran.bind("<Button-1>", self.on_click)


        ##DESSINER LES IMAGES
        self.dessiner_carte()

    def contient_troupe(self, region: Region) -> bool:
        return region.contient_troupe is not None

    def contient_batiment(self, region: Region) -> bool:
        return region.batiment is not None

    def dessiner_carte(self):
        global TAILLE_TUILES
        print(self.carte.nb_ligne, self.carte.nb_colonne)
        for i in range(self.carte.nb_ligne):
            ligne = self.carte.grille[i]
            for j in range(self.carte.nb_colonne):
                region = ligne[j]
                print(j*TAILLE_TUILES, i*TAILLE_TUILES)
                self.ecran.create_image(
                    j*TAILLE_TUILES, i*TAILLE_TUILES,
                    image=self.images[region.environnement],
                    anchor=NW)

    """def dessiner_unites(self):
        for ligne in range(self.carte.nb_ligne):
            for colonne in range(self.carte.nb_colonne):
                if self.contient_troupe(self.carte.grille[ligne][colonne]):
                    self.ecran.create_image(
                        colonne*TAILLE_TUILES, ligne*TAILLE_TUILES,
                        image=self.images["soldat"],
                        anchor=NW,
                        tag="SOLDAT"
                    )

    def effacer_unites(self):
        self.ecran.delete("SOLDAT")

    def dessiner_batiments(self):
        for ligne in range(self.carte.nb_ligne):
            for colonne in range(self.carte.nb_colonne):
                if self.contient_batiment(self.carte.grille[ligne][colonne]):
                    self.ecran.create_image(
                        colonne*TAILLE_TUILES, ligne*TAILLE_TUILES,
                        image=self.images["batiment"],
                        anchor=NW,
                        tag="BATIMENT"
                    )

    def effacer_batiments(self):
        self.ecran.delete("BATIMENT")"""

    def on_click(self, event:Event):
        x = event.x // TAILLE_TUILES
        y = event.y // TAILLE_TUILES
        print("Clic")


if __name__ == '__main__':
    ma_carte = Carte(5, 5)

    fenetre = Tk()
    zone_carte = InterfaceCarte(fenetre, ma_carte)
    zone_carte.ecran.grid(row=10, column=10)

    """zone_carte.dessiner_unites()
    zone_carte.dessiner_batiments()"""

    fenetre.mainloop()

