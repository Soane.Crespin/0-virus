from soldat import *
import json
from random import choice


def charger_donnees_terrains()->dict:
    fichier = open(file="Data/environnement.json", mode="r")
    mesDonnees = json.load(fichier)
    fichier.close()
    return mesDonnees

def charger_noms_terrains()->list:
    return list(charger_donnees_terrains().keys())

class Region:
    def __init__(self, environnement:str):
        DICO_ENVIRONNEMENTS = charger_donnees_terrains()

        self.batiment      = None           #Pas de batiments par defaut
        self.troupe        = None           #Pas d'unites par defaut

        self.environnement = environnement
        self.argent        = DICO_ENVIRONNEMENTS[environnement]["argent"]
        self.nom           = DICO_ENVIRONNEMENTS[environnement]["nom"]
        self.bois          = DICO_ENVIRONNEMENTS[environnement]["bois"]
        self.pierre        = DICO_ENVIRONNEMENTS[environnement]["pierre"]
        self.nourriture    = DICO_ENVIRONNEMENTS[environnement]["nourriture"]

    ##### FONCTIONS POUR AFFICHAGE #####

    def __str__(self):
        txt = "batiment:" +str(self.batiment) + "\n" #Le \n c'est le retour à la ligne
        txt += "nourriture :" +str(self.nourriture) + "\n"
        txt += "bois :" + str(self.bois) + "\n"
        txt += "pierre:"+str(self.pierre) +"\n"
        txt += "argent:"+str(self.argent)
        txt += str(self.environnement) + "\n"
        return txt

    def __repr__(self):
        return self.nom

    ##### FONCTIONS POUR BATIMENTS #####

    def contient_batiment(self)->bool:
        """Renvoie True si la region contient un bâtiment, False sinon"""
        if self.batiment==None:
            return False
        else:
            return True

    def ajouter_batiment(self, bat:str):
        """ajoute un batiment à la region"""
        if not self.contient_batiment():
            self.batiment = bat
        else :
            pass

    def supprimer_batiment(self):
        """Supprimer un batiment d'une case"""
        self.batiment = None                #A reinitialiser, comme dans __init__()


    def recolter(self):
        """Ce que recolte un batiment en fonction de l'environnement"""
        if self.batiment != None :          #OU if self.contient_batiment():
            return self.batiment.recolter()
        else :
            return None                     #S'il n'y a pas de batiment

    ##### FONCTIONS POUR TROUPES #####

    def contient_troupe(self)->bool:
        """Renvoie True si la region contient une troupe, False sinon"""
        if self.troupe== None:
            return False
        else:
            return True

    def ajouter_troupe(self, troupe):
        if not self.contient_troupe():
            self.troupe = troupe

    def supprimer_troupe(self):
        if self.contient_troupe():
            self.troupe = None

    def envoyer_troupe_vers(self, other):
        """Deplace la troupe de la region self vers la region other
        WARNING : regarder si les cases sont vides ou non"""
        if self.contient_troupe() == True and other.contient_troupe() ==False:
            other.ajouter_troupe(self.troupe)
            self.supprimer_troupe()


#########################
### FONCTIONS ANNEXES ###
#########################

def region_aleatoire():
    environnement = choice(charger_noms_terrains())
    return Region(environnement)



############
### MAIN ###
############


if __name__ == '__main__':
    r1 = Region("montagne")
    print(r1.environnement, "contient", r1.bois, "bois.")
    r2 = Region("foret")
    print(r2.environnement, "contient", r2.bois, "bois.")

    u = Soldat("chevalier")
    r1.ajouter_troupe(u)
    print(r1.troupe, r2.troupe)
    r1.envoyer_troupe_vers(r2)
    print(r1.troupe, r2.troupe)



