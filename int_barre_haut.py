﻿from tkinter import *
from ressources import *


class BarreHaut():
    def __init__(self, master, ressources):
        self.frame = Frame(master)
        self.ressources = ressources
        ##Création des Labels (on peut aussi ne pas écrire les text= et lancer la fonction maj_label() )
        self.img_food = PhotoImage(file = 'Icon/cerise.png')
        self.lbl_food = Label(
            master = self.frame,
            text = "Nourriture : " + str(self.ressources.stock_nourriture),
            image = self.img_food,
            compound = LEFT
        )
        self.img_pierre = PhotoImage(file = 'Icon/cristaux.png')
        self.lbl_stone = Label(
            master = self.frame,
            text = "Pierre : " + str(self.ressources.stock_pierre),
            image = self.img_pierre,
            compound = LEFT
        )
        self.img_argent = PhotoImage(file = 'Icon/argent.png')
        self.lbl_silver = Label(
            master = self.frame,
            text = "Argent : " + str(self.ressources.stock_argent),
            image = self.img_argent,
            compound = LEFT
        )
        self.img_bois = PhotoImage(file = 'Icon/labyrinthe.png')
        self.lbl_wood = Label(
            master = self.frame,
            text = "Bois : " + str(self.ressources.stock_bois),
            image = self.img_bois,
            compound = LEFT
        )


        self.dessiner_widgets()


    def dessiner_widgets(self):
        self.lbl_wood.grid(row = 0,   column = 0 )
        self.lbl_silver.grid(row = 0, column = 1 )
        self.lbl_stone.grid(row = 0,  column = 2 )
        self.lbl_food.grid(row = 0,  column  = 3 )


    def maj(self)->str:
        """met à jour les Label en cas de modification"""
        bois = "Bois : " + str(self.ressources.stock_bois)
        self.lbl_wood.config(text = bois)

        argent = "Argent : " + str(self.ressources.stock_argent)
        self.lbl_silver.config(text = argent)

        pierre = "Pierre : " + str(self.ressources.stock_pierre)
        self.lbl_stone.config(text = pierre)

        nourriture = "Nourriture : " + str(self.ressources.stock_nourriture)
        self.lbl_food.config(text = nourriture)

        self.change_couleur()

    def change_couleur(self):
        """Pourquoi ne pas mettre en rouge les ressources vides ou trop faibles ?"""
        if self.ressources.stock_bois <= 100 :
            self.lbl_wood.config(fg = "red", text = "Bois : " + str(self.ressources.stock_bois))
        if self.ressources.stock_argent <= 100 :
            self.lbl_silver.config(fg = "red", text = "Argent : " + str(self.ressources.stock_argent))
        if self.ressources.stock_pierre <= 100 :
            self.lbl_stone.config(fg = "red", text = "Pierre : " + str(self.ressources.stock_pierre))
        if self.ressources.stock_nourriture <= 100 :
            self.lbl_food.config(fg = "red", text = "Nourriture : " + str(self.ressources.stock_nourriture))


############
### MAIN ###
############

if __name__ == '__main__':
    def plus_pierre(ress, interf):
        ress.ajouter_pierre(10)
        interf.maj()

    def moins_pierre(ress, interf):
        ress.retirer_pierre(10)
        interf.maj()

    mes_ressources = Ressources()

    fenetre = Tk()
    barre_haut = BarreHaut(
        fenetre,
        mes_ressources
        )
    btn_plus_pierre = Button(
        fenetre,
        text="Ajouter 10 pierres",
        command = lambda : plus_pierre(mes_ressources, barre_haut)
        )
    btn_moins_pierre = Button(
        fenetre,
        text="Retirer 10 pierres",
        command = lambda : moins_pierre(mes_ressources, barre_haut)
        )

    barre_haut.frame.grid(row = 1, column = 0, columnspan = 2)
    btn_plus_pierre.grid(row = 0, column = 0)
    btn_moins_pierre.grid(row = 0, column = 1)

    fenetre.mainloop()


