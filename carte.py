from region import *
from batiment import*
from soldat import*
from random import choice

exemple = [
    ["montagne", "foret", "plaine","lac"],
    ["montagne", "lac", "montagne", "foret"]
    ]
list_bat=["QG","Carriere","Scierie","Mine","Caserne","Maison","Ferme","Laboratoire"]
list_trp=["chevalier","archer","legionnaire"]
class Carte:
    def __init__(self, nb_ligne, nb_colonne):
        self.grille     = []                            # Creation de la map sous forme de tableau
        self.nb_ligne   = nb_ligne                      #ou self.hauteur
        self.nb_colonne = nb_colonne                    #ou self.largeur
        self.generer_carte_aleatoire()


    def generer_carte_aleatoire(self):
        """Cree une carte aleatoire"""
        for num_ligne in range(self.nb_ligne):
            ligne= []
            for num_colonne in range(self.nb_colonne):
                environnement = choice(["montagne", "foret", "plaine", "lac"])             #On tire un environnement au hasard
                region = Region(environnement)

                                                      #On cree la region avec
                ligne.append(region)                                                #on l'ajoute aÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â  la carte
            self.grille.append(ligne)

    def importer_carte(self, tab:list):
        """genere une carte aÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â  partir d'un dictionnaire ou d'un tableau
        contenant les environnements. Exemple ci-dessous."""
        ligne=[]
        for i in range(len(tab)):
            for j in range(len(tab[i])):
                ligne.append(tab[i][j])
            self.grille.append(ligne)


    def __str__(self):
        """Renvoyer un texte avec les environnements des regions"""
        nb_montagne=0
        nb_foret=0
        nb_plaine=0
        nb_lac=0
        for ligne in range(len(self.grille)):
            for colonne in range(ligne):
                if self.grille[ligne][colonne].environnement == "montagne":
                    nb_montagne+=1
                if self.grille[ligne][colonne].environnement== "foret":
                    nb_foret+=1
                if self.grille[ligne][colonne].environnement== "plaine":
                    nb_plaine+=1
                if self.grille[ligne][colonne].environnement== "lac":
                    nb_lac+=1

        txt= "Il y a "+ str(nb_montagne)+ " montagnes.""\n"
        txt+= "Il y a "+ str(nb_foret)+ " forets.""\n"
        txt+= "Il y a "+ str(nb_plaine)+ " plaines.""\n"
        txt+= "Il y a "+ str(nb_lac)+ " lacs."

        return txt

    def ajouter_troupe(self, ligne, colonne, unit):
        if self.grille[ligne][colonne].environnement!="lac":
            if self.contient_troupe(ligne,colonne)==False:
                self.grille[ligne][colonne].troupe=unit


    def contient_troupe(self, ligne, colonne)->bool:

        if self.grille[ligne][colonne].troupe in list_trp:
            return True
        else:
            return False

    def coord_bat(self,bat):
        for i in range(nb_ligne):
            for j in range(nb_colonne):
                if self.grille[i][j].batiment==bat:
                    return (i,j)


    def contient_batiment(self, ligne, colonne)->bool:

        if self.grille[ligne][colonne].batiment in list_bat:
            return True
        else:
            return False

    def retirer_batiment(self, ligne, colonne):
        if self.contient_batiment(ligne,colonne)==True:
            self.grille[ligne][colonne].batiment==None

    def retirer_troupe(self, ligne, colonne):
        if self.contient_troupe(ligne,colonne)==True:
            self.grille[ligne][colonne].troupe==None

    def deplacer_troupe(self, depart:tuple, arrivee:tuple):
        if self.grille[arrivee[0]][arrivee[1]].environnement!="lac":

            if self.contient_troupe(depart[0],depart[1])==True:
                if self.contient_troupe(arrivee[0],arrivee[1])==False:
                    self.grille[arrivee[0]][arrivee[1]].troupe=self.grille[depart[0]][depart[1]].troupe
                    self.grille[depart[0]][depart[1]].troupe= None




    def ajouter_batiment(self, ligne, colonne,bat):
        if self.grille[ligne][colonne].environnement == bat.environnement:
            if self.contient_batiment(ligne,colonne)==False:
                self.grille[ligne][colonne].batiment = bat



############
### MAIN ###
############


if __name__ == '__main__':
    c = Carte(5,8)

    """c.generer_carte_aleatoire()


    tab = [
        ["montagne", "foret", "foret", "lac"],
        ["foret", "lac", "foret", "montagne"]
        ]


    c.importer_carte(tab)"""

    c.generer_carte_aleatoire()
    c.ajouter_batiment(0,1,Batiment("Mine"))
    c.ajouter_troupe(0,1,"chevalier")
    #c.ajouter_batiment(0,0,"Scierie")
    print(c)

    print("La case de la ligne 0 colonne 1 contient des bats :", c.contient_batiment(0,1))
    print(c.contient_troupe(0,1))
    c.deplacer_troupe((0,1),(1,2))
    print(c.contient_troupe(0,1))
    print(c.contient_troupe(1,2))